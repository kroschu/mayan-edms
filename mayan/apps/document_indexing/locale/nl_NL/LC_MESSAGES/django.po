# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Evelijn Saaltink <evelijnsaaltink@gmail.com>, 2016
# Johan Braeken, 2017
# Lucas Weel <ljj.weel@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-26 22:22-0400\n"
"PO-Revision-Date: 2018-09-12 07:46+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Dutch (Netherlands) (http://www.transifex.com/rosarior/mayan-edms/language/nl_NL/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl_NL\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin.py:24
msgid "None"
msgstr "Geen"

#: admin.py:26 links.py:67 models.py:50
msgid "Document types"
msgstr "Documentsoorten"

#: apps.py:51
msgid "Document indexing"
msgstr "Documentindexering"

#: apps.py:83 models.py:34
msgid "Label"
msgstr "Label"

#: apps.py:84 models.py:39
msgid "Slug"
msgstr ""

#: apps.py:86 apps.py:112 models.py:47 models.py:184
msgid "Enabled"
msgstr "Ingeschakeld"

#: apps.py:93
msgid "Total levels"
msgstr ""

#: apps.py:99
msgid "Total documents"
msgstr ""

#: apps.py:108 apps.py:125 apps.py:142
msgid "Level"
msgstr "Niveau"

#: apps.py:118
msgid "Has document links?"
msgstr "Heeft document verwijzingen?"

#: apps.py:129 apps.py:148
msgid "Levels"
msgstr ""

#: apps.py:133 apps.py:152 models.py:301
msgid "Documents"
msgstr "Documenten"

#: forms.py:17
msgid "Indexes to be queued for rebuilding."
msgstr ""

#: forms.py:18 links.py:25 links.py:31 links.py:39 links.py:43 models.py:58
#: views.py:88 views.py:249
msgid "Indexes"
msgstr "Indexeringen"

#: handlers.py:20
msgid "Creation date"
msgstr "Aanmaakdatum"

#: links.py:47 views.py:38
msgid "Create index"
msgstr "Indexering aanmaken"

#: links.py:52 links.py:85
msgid "Edit"
msgstr "Bewerken"

#: links.py:58 links.py:90
msgid "Delete"
msgstr "Verwijderen"

#: links.py:62
msgid "Tree template"
msgstr "Sjabloon boomstructuur"

#: links.py:76
msgid "Deletes and creates from scratch all the document indexes."
msgstr "Documentindexeringen vanaf nul vernieuwen"

#: links.py:78 views.py:370
msgid "Rebuild indexes"
msgstr "Opnieuw indexeren"

#: links.py:81
msgid "New child node"
msgstr "Nieuwe node"

#: models.py:38
msgid "This value will be used by other apps to reference this index."
msgstr ""

#: models.py:44
msgid ""
"Causes this index to be visible and updated when document data changes."
msgstr "Maakt deze index zichtbaar en 'up-to-date' wanneer document gegevens wijzigd."

#: models.py:57 models.py:168
msgid "Index"
msgstr "Index"

#: models.py:140
msgid "Index instance"
msgstr "index instance"

#: models.py:141
msgid "Index instances"
msgstr ""

#: models.py:172
msgid ""
"Enter a template to render. Use Django's default templating language "
"(https://docs.djangoproject.com/en/1.11/ref/templates/builtins/)"
msgstr ""

#: models.py:176
msgid "Indexing expression"
msgstr "Indexeringsexpressie"

#: models.py:181
msgid "Causes this node to be visible and updated when document data changes."
msgstr "Maakt deze node zichtbaar en 'up-to-date' wanneer document gegevens wijzigen"

#: models.py:189
msgid ""
"Check this option to have this node act as a container for documents and not"
" as a parent for further nodes."
msgstr "Selecteer deze optie, wanneer deze node alleen documenten dient te bevatten. "

#: models.py:192
msgid "Link documents"
msgstr "Koppel documenten"

#: models.py:196
msgid "Index node template"
msgstr ""

#: models.py:197
msgid "Indexes node template"
msgstr ""

#: models.py:201
msgid "Root"
msgstr ""

#: models.py:257
#, python-format
msgid ""
"Error indexing document: %(document)s; expression: %(expression)s; "
"%(exception)s"
msgstr "Fout bij het indexeren van document: %(document)s; uitdrukking: %(expression)s; %(exception)s"

#: models.py:294
msgid "Index template node"
msgstr ""

#: models.py:297
msgid "Value"
msgstr "Waarde"

#: models.py:307
msgid "Index node instance"
msgstr ""

#: models.py:308
msgid "Indexes node instances"
msgstr ""

#: models.py:406
msgid "Document index node instance"
msgstr ""

#: models.py:407
msgid "Document indexes node instances"
msgstr ""

#: permissions.py:7 queues.py:8
msgid "Indexing"
msgstr "Indexering bezig"

#: permissions.py:10
msgid "Create new document indexes"
msgstr "Aanmaken van nieuw document-indexeringen"

#: permissions.py:13
msgid "Edit document indexes"
msgstr "Wijzig document-indexeringen"

#: permissions.py:16
msgid "Delete document indexes"
msgstr "Verwijder document-indexeringen"

#: permissions.py:20
msgid "View document index instances"
msgstr ""

#: permissions.py:23
msgid "View document indexes"
msgstr "Bekijk document-indexeringen"

#: permissions.py:26
msgid "Rebuild document indexes"
msgstr "documenten opnieuw indexeren"

#: queues.py:12
msgid "Delete empty index nodes"
msgstr ""

#: queues.py:16
msgid "Remove document"
msgstr ""

#: queues.py:20
msgid "Index document"
msgstr ""

#: queues.py:24
msgid "Rebuild index"
msgstr ""

#: views.py:53
#, python-format
msgid "Delete the index: %s?"
msgstr ""

#: views.py:66
#, python-format
msgid "Edit index: %s"
msgstr ""

#: views.py:82
msgid ""
"Indexes group document automatically into levels. Indexe are defined using "
"template whose markers are replaced with direct properties of documents like"
" label or description, or that of extended properties like metadata."
msgstr ""

#: views.py:87
msgid "There are no indexes."
msgstr ""

#: views.py:94
msgid "Available document types"
msgstr "Beschikbare documentsoorten"

#: views.py:96
msgid "Document types linked"
msgstr ""

#: views.py:111
#, python-format
msgid "Document types linked to index: %s"
msgstr ""

#: views.py:114
msgid ""
"Only the documents of the types selected will be shown in the index when "
"built. Only the events of the documents of the types select will trigger "
"updates in the index."
msgstr ""

#: views.py:147
#, python-format
msgid "Tree template nodes for index: %s"
msgstr ""

#: views.py:177
#, python-format
msgid "Create child node of: %s"
msgstr ""

#: views.py:201
#, python-format
msgid "Delete the index template node: %s?"
msgstr ""

#: views.py:223
#, python-format
msgid "Edit the index template node: %s?"
msgstr ""

#: views.py:244
msgid ""
"This could mean that no index templates have been created or that there "
"index templates but they are no properly defined."
msgstr ""

#: views.py:248
msgid "There are no index instances available."
msgstr ""

#: views.py:290
#, python-format
msgid "Navigation: %s"
msgstr ""

#: views.py:295
#, python-format
msgid "Contents for index: %s"
msgstr ""

#: views.py:349
msgid ""
"Assign the document type of this document to an index to have it appear in "
"instances of those indexes organization units. "
msgstr ""

#: views.py:354
msgid "This document is not in any index"
msgstr ""

#: views.py:358
#, python-format
msgid "Indexes nodes containing document: %s"
msgstr ""

#: views.py:384
#, python-format
msgid "%(count)d index queued for rebuild."
msgid_plural "%(count)d indexes queued for rebuild."
msgstr[0] ""
msgstr[1] ""
